<?php declare(strict_types=1);


namespace ScandiTutorials\CustomWidget\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;


class NewsletterWidget extends Template implements BlockInterface
{
    protected $_template = "widget/newsletter_widget.phtml";
}
