// import PropTypes from 'prop-types';
import { PureComponent } from 'react';

import './PositiveAffirmations.style';

/** @namespace TutorialCsaApp/Component/PositiveAffirmations/Component/PositiveAffirmationsComponent */
export class PositiveAffirmationsComponent extends PureComponent {
    static propTypes = {
    };

    render() {
        return (
            <div block="PositiveAffirmations">
                <p>{ __('Today, I am brimming with energy and overflowing with joy.') }</p>
                <p>{ __('I live in the moment while learning from the past and preparing for the future.') }</p>
                <p>{ __('Life is beautiful.') }</p>
            </div>
        );
    }
}

export default PositiveAffirmationsComponent;
