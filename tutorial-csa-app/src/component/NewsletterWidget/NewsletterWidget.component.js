import propTypes from 'prop-types';
import { PureComponent } from 'react';

import NewsletterSubscription from 'Component/NewsletterSubscription';

import './NewsletterWidget.style';

/** @namespace TutorialCsaApp/Component/NewsletterWidget/Component/NewsletterWidgetComponent */
export class NewsletterWidgetComponent extends PureComponent {
    static propTypes = {
        'data-title': propTypes.string.isRequired
    };

    render() {
        const { 'data-title': title } = this.props;

        return (
            <div block="NewsletterWidget">
                <h2>{ title }</h2>
                <NewsletterSubscription />
            </div>
        );
    }
}

export default NewsletterWidgetComponent;
