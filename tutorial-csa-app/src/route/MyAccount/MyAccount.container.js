import { connect } from 'react-redux';

import {
    mapDispatchToProps,
    mapStateToProps,
    MyAccountContainer as SourceMyAccountContainer
} from 'SourceRoute/MyAccount/MyAccount.container';
import {
    ADDRESS_BOOK,
    DASHBOARD,
    MY_DOWNLOADABLE,
    MY_ORDERS,
    MY_WISHLIST,
    NEWSLETTER_SUBSCRIPTION
} from 'Type/Account';

import { POSITIVE_AFFIRMATIONS } from './MyAccount.component';

export {
    mapStateToProps,
    mapDispatchToProps
};

/** @namespace TutorialCsaApp/Route/MyAccount/Container/MyAccountContainer */
export class MyAccountContainer extends SourceMyAccountContainer {
    tabMap = {
        [DASHBOARD]: {
            url: '/dashboard',
            name: __('Dashboard')
        },
        [ADDRESS_BOOK]: {
            url: '/address-book',
            name: __('Address book')
        },
        [MY_ORDERS]: {
            url: '/my-orders',
            name: __('My orders')
        },
        [MY_DOWNLOADABLE]: {
            url: '/my-downloadable',
            name: __('My downloadable')
        },
        [MY_WISHLIST]: {
            url: '/my-wishlist',
            name: __('My wishlist'),
            headerTitle: () => this.getMyWishlistHeaderTitle()
        },
        [NEWSLETTER_SUBSCRIPTION]: {
            url: '/newsletter-subscription',
            name: __('Newsletter Subscription')
        },
        [POSITIVE_AFFIRMATIONS]: {
            url: '/affirmations',
            name: __('Positive Affirmations')
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(MyAccountContainer);
