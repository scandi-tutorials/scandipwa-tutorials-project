import MyAccountAddressBook from 'Component/MyAccountAddressBook';
import MyAccountDashboard from 'Component/MyAccountDashboard';
import MyAccountDownloadable from 'Component/MyAccountDownloadable';
import MyAccountMyOrders from 'Component/MyAccountMyOrders';
import MyAccountMyWishlist from 'Component/MyAccountMyWishlist';
import MyAccountNewsletterSubscription from 'Component/MyAccountNewsletterSubscription';
import PositiveAffirmations from 'Component/PositiveAffirmations';
import {
    MyAccount as SourceMyAccount
} from 'SourceRoute/MyAccount/MyAccount.component';
import {
    ADDRESS_BOOK,
    DASHBOARD,
    MY_DOWNLOADABLE,
    MY_ORDERS,
    MY_WISHLIST,
    NEWSLETTER_SUBSCRIPTION
} from 'Type/Account';

export const POSITIVE_AFFIRMATIONS = 'affirmations';

/** @namespace TutorialCsaApp/Route/MyAccount/Component/MyAccountComponent */
export class MyAccountComponent extends SourceMyAccount {
    renderMap = {
        [DASHBOARD]: MyAccountDashboard,
        [MY_ORDERS]: MyAccountMyOrders,
        [MY_WISHLIST]: MyAccountMyWishlist,
        [ADDRESS_BOOK]: MyAccountAddressBook,
        [NEWSLETTER_SUBSCRIPTION]: MyAccountNewsletterSubscription,
        [MY_DOWNLOADABLE]: MyAccountDownloadable,
        [POSITIVE_AFFIRMATIONS]: PositiveAffirmations
    };
}

export default MyAccountComponent;
