import {
    PRODUCT_ATTRIBUTES,
    PRODUCT_INFORMATION,
    PRODUCT_REVIEWS
} from 'Route/ProductPage/ProductPage.config';
import {
    ProductPage as SourceProductPage
} from 'SourceRoute/ProductPage/ProductPage.component';

import './ProductPage.override.style.scss';

/** @namespace TutorialCsaApp/Route/ProductPage/Component/ProductPageComponent */
export class ProductPageComponent extends SourceProductPage {
    tabMap = {
        [PRODUCT_INFORMATION]: {
            name: __('About'),
            shouldTabRender: () => {
                const { isInformationTabEmpty } = this.props;
                return isInformationTabEmpty;
            },
            render: (key) => this.renderProductInformationTab(key)
        },
        [PRODUCT_ATTRIBUTES]: {
            name: __('Details'),
            shouldTabRender: () => {
                const { isAttributesTabEmpty } = this.props;
                return isAttributesTabEmpty;
            },
            render: (key) => this.renderProductAttributesTab(key)
        },
        GUARANTEE_TAB: {
            name: __('Guarantee'),
            shouldTabRender: () => false,
            render: (key) => this.renderGuarantee(key)
        },
        [PRODUCT_REVIEWS]: {
            name: __('Reviews'),
            // Return false since it always returns 'Add review' button
            shouldTabRender: () => false,
            render: (key) => this.renderProductReviewsTab(key)
        }
    };

    renderGuarantee(key) {
        return (
            <section block="ProductPage" elem="Guarantee" key={ key }>
                <h1>{ __('Our Promise to You') }</h1>
                <p>
                    { __('Everything we make is guaranteed to work for at least two years. It cannot break. If it'
                        + ' breaks it\'s probably your fault.') }
                </p>
            </section>
        );
    }
}

export default ProductPageComponent;
