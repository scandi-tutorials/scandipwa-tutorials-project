// import PropTypes from 'prop-types';
import { PureComponent } from 'react';

import './AboutUs.style';

/** @namespace TutorialCsaApp/Route/AboutUs/Component/AboutUsComponent */
export class AboutUsComponent extends PureComponent {
    static propTypes = {
        // TODO: implement prop-types
    };

    render() {
        return (
            <div block="AboutUs">
                <h1>Don&apos;t Panic</h1>
                <p>
                    Welcome to my website.
                </p>
            </div>
        );
    }
}

export default AboutUsComponent;
