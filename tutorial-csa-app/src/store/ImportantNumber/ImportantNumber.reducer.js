// TODO update this import when renamed
import { IMPORTANTNUMBER_ACTION_TYPE } from './ImportantNumber.action';

/** @namespace TutorialCsaApp/Store/ImportantNumber/Reducer/getInitialState */
export const getInitialState = () => ({
    number: 42
});

/** @namespace TutorialCsaApp/Store/ImportantNumber/Reducer/ImportantNumberReducer */
export const ImportantNumberReducer = (state = getInitialState(), action) => {
    switch (action.type) {
    case IMPORTANTNUMBER_ACTION_TYPE:
        // const { payload } = action;

        return {
            ...state
        };

    default:
        return state;
    }
};

export default ImportantNumberReducer;
