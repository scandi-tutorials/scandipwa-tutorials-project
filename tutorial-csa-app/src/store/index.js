import { getStaticReducers as baseGetStaticReducers } from 'SourceStore/index';
import ImportantNumberReducer from 'Store/ImportantNumber/ImportantNumber.reducer';

/** @namespace TutorialCsaApp/Store/Index/getStaticReducers */
export const getStaticReducers = () => ({
    ...baseGetStaticReducers(),
    ImportantNumberReducer
});

export default function injectStaticReducers(store) {
    // Inject all the static reducers into the store
    Object.entries(getStaticReducers()).forEach(
        ([name, reducer]) => store.injectReducer(name, reducer)
    );

    return store;
}
